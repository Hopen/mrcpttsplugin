// MrcpTtsPluginTests.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <YandexTts.h>

int main()
{
    auto str = Utils::string_to_wstring("�������� ��������� ��� �������� ������ ���������� ���� ������");
    auto requestText = Utils::to_utf8(str);

    //auto requestText = Utils::cp1251_to_utf8("�������� ��������� ��� �������� ������ ���������� ���� ������");

    std::string channelId{ "channel123" };

    auto stream = std::make_unique<Yandex::TtsStream>();
    stream->SetRequestId(channelId.c_str(), channelId.size());
    stream->MakeRequest(requestText.c_str());

    std::string bufferStr;

    if (stream->IsSuccessed())
    {
        size_t streamSize = stream->GetStreamSize();

        while (true)
        {
            size_t bufferSize = 1024;
            char buffer[1024] = {};

            size_t readSize = stream->ReadStream(buffer, bufferSize);
            bufferStr.append(buffer, readSize);

            if (readSize != bufferSize)
            {
                break;
            }
        }

        std::cout << "Stream size: " << streamSize << std::endl;

        std::ofstream f("c:\input.raw", std::ios_base::binary);
        f << bufferStr;
        f.close();


        std::cout << "Read size: " << bufferStr.size() << std::endl;
    }
    else
    {
        std::cout << "Error" << std::endl;
    }
}