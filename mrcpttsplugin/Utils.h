#pragma once

#include <string>
#include <iostream>
#include <fstream>
#include <cvt/wstring>
#include <functional>
#include <codecvt>
//#include "windows.h"

namespace Utils
{
    inline std::wstring stow(const std::string& in)
    {
        typedef std::codecvt_utf8<wchar_t> convert_typeX;
        std::wstring_convert<convert_typeX, wchar_t> converterX;
        return converterX.from_bytes(in);
    }

    inline std::string wtos(const std::wstring& in)
    {
        typedef std::codecvt_utf8<wchar_t> convert_typeX;
        std::wstring_convert<convert_typeX, wchar_t> converterX;
        return converterX.to_bytes(in);
    }

    inline std::wstring from_utf8(const std::string& _val)
    {
        std::wstring_convert<std::codecvt_utf8<wchar_t>> conv;
        return conv.from_bytes(_val);
    }

    inline std::string to_utf8(const std::wstring& _val)
    {
        std::wstring_convert<std::codecvt_utf8<wchar_t>> conv;
        return conv.to_bytes(_val);
    }

    inline std::wstring utf8ToUtf16(const std::string& utf8Str)
    {
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> conv;
        return conv.from_bytes(utf8Str);
    }

    inline std::string utf16ToUtf8(const std::wstring& utf16Str)
    {
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> conv;
        return conv.to_bytes(utf16Str);
    }

    inline std::wstring string_to_wstring(std::string str) {
        std::wstring convertedString;
        int requiredSize = MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, 0, 0);
        if (requiredSize > 0) {
            std::vector<wchar_t> buffer(requiredSize);
            MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, &buffer[0], requiredSize);
            convertedString.assign(buffer.begin(), buffer.end() - 1);
        }
        return convertedString;
    }

    inline std::string cp1251_to_utf8(const char* str) {
        std::string res;
        int result_u, result_c;
        result_u = MultiByteToWideChar(1251, 0, str, -1, 0, 0);
        if (!result_u) { return 0; }
        wchar_t* ures = new wchar_t[result_u];
        if (!MultiByteToWideChar(1251, 0, str, -1, ures, result_u)) {
            delete[] ures;
            return 0;
        }
        result_c = WideCharToMultiByte(65001, 0, ures, -1, 0, 0, 0, 0);
        if (!result_c) {
            delete[] ures;
            return 0;
        }
        char* cres = new char[result_c];
        if (!WideCharToMultiByte(65001, 0, ures, -1, cres, result_c, 0, 0)) {
            delete[] cres;
            return 0;
        }
        delete[] ures;
        res.append(cres);
        delete[] cres;
        return res;
    }

    namespace Detail
    {
        static std::string UnicodeToMultibyte(const std::wstring& src, UINT codePage)
        {
            std::string strTo;
            if (src.empty())
            {
                return strTo;
            }

            const auto sizeNeeded = ::WideCharToMultiByte(codePage, 0, src.data(), static_cast<int>(src.size()), NULL, 0, NULL, NULL);
            strTo.resize(sizeNeeded, 0);
            ::WideCharToMultiByte(codePage, 0, src.data(), static_cast<int>(src.size()), &strTo[0], sizeNeeded, NULL, NULL);
            return strTo;
        }

        static std::wstring MultibyteToUnicode(const std::string& src, UINT codePage)
        {
            std::wstring strTo;
            if (src.empty())
            {
                return strTo;
            }

            const auto sizeNeeded = ::MultiByteToWideChar(codePage, 0, src.data(), static_cast<int>(src.size()), NULL, 0);
            strTo.resize(sizeNeeded, 0);
            ::MultiByteToWideChar(codePage, 0, src.data(), static_cast<int>(src.size()), &strTo[0], sizeNeeded);
            return strTo;
        }
    }

    inline std::string UTF16ToUTF8(const std::wstring& src)
    {
        return Detail::UnicodeToMultibyte(src, CP_UTF8);
    }

    inline std::wstring UTF8ToUTF16(const std::string& src)
    {
        return Detail::MultibyteToUnicode(src, CP_UTF8);
    }


    template <class TReadFunction>
    void GetFileStream(const std::string& aAudioPath, TReadFunction read)
    {
        int length;
        char* buffer;

        std::ifstream is;
        is.open(aAudioPath, std::ios::binary);
        // get length of file:
        is.seekg(0, std::ios::end);
        length = is.tellg();
        is.seekg(0, std::ios::beg);
        // allocate memory:
        buffer = new char[length];
        // read data as a block:
        is.read(buffer, length);

        is.close();

        read(buffer, length);
        delete[] buffer;
    }

}