extern "C" {
#include "YandexTtsApi.h"
}

#include "YandexTts.h"

template <class T, class TReturnValue>
static TReturnValue* ConvertToYandexCloudService(T* aService)
{
    TReturnValue* service = reinterpret_cast<TReturnValue*>(aService);
    if (!service)
    {
        throw std::runtime_error("reinterpret_cast failed");
    }
    return service;
}

yandex_tts_stream_t* create_tts_stream()
{
    return reinterpret_cast<yandex_tts_stream_t*>(new Yandex::TtsStream{});
}

void destroy_tts_stream(yandex_tts_stream_t* aStream)
{
    auto stream = ConvertToYandexCloudService<yandex_tts_stream_t, Yandex::TtsStream>(aStream);
    delete stream;
}

void set_channel_id(yandex_tts_stream_t* aStream, const char* aChennelId, int aSize)
{
    auto stream = ConvertToYandexCloudService<yandex_tts_stream_t, Yandex::TtsStream>(aStream);
    stream->SetRequestId(aChennelId, aSize);
}

void make_tts_request(yandex_tts_stream_t* aStream, const char* aRequest, int aSize)
{
    auto stream = ConvertToYandexCloudService<yandex_tts_stream_t, Yandex::TtsStream>(aStream);
    stream->MakeRequest(std::string{aRequest, static_cast<size_t>(aSize) });
}

int read_tts_stream(yandex_tts_stream_t* aStream, void* aBuffer, int aSize)
{
    auto stream = ConvertToYandexCloudService<yandex_tts_stream_t, Yandex::TtsStream>(aStream);
    return static_cast<int>(stream->ReadStream(static_cast<char*>(aBuffer), static_cast<size_t>(aSize)));
}

int is_tts_request_successed(yandex_tts_stream_t* aStream)
{
    auto stream = ConvertToYandexCloudService<yandex_tts_stream_t, Yandex::TtsStream>(aStream);
    return stream->IsSuccessed() ? TRUE : FALSE;
}

extern const char* get_sample_rate_hertz(yandex_tts_stream_t* aStream)
{
    auto stream = ConvertToYandexCloudService<yandex_tts_stream_t, Yandex::TtsStream>(aStream);
    return stream->GetSampleRateHertz();
}