#pragma once


#include <iostream>
#include <string>
#include <sstream>
#include <curl/curl.h>
#include <assert.h>
#include <fstream>

#include <boost/filesystem.hpp>

#include "Configuration/SystemConfiguration.h"
#include "Utils.h"

const size_t HttpSuccessErrorCode = 200;

inline int debug_callback(CURL* handle,
    curl_infotype type,
    char* data,
    size_t size,
    void* userdata)
{
    if (type == CURLINFO_HEADER_OUT)
    {
        std::stringstream* s = (std::stringstream*)userdata;
        s->write(data, size);
    }
    return CURLE_OK;
}

inline size_t write_callback(void* ptr, size_t size, size_t nmemb, void* userdata)
{
    std::stringstream* s = (std::stringstream*)userdata;
    size_t fullSize = size * nmemb;
    s->write(static_cast<const char*>(ptr), fullSize);
    return fullSize;
}

namespace Yandex
{
    struct YandexApiConfig
    {
        std::string ApiKey;
        std::string Url;
        std::string FolderId;
        bool SaveStream = false;
        std::string RawFolder;
        std::string SampleRateHertz;
        std::string Lang;
    };

    class TtsStream
    {
    public:
        TtsStream()
        {
            Initialize();
        }

        void SetRequestId(const char* aChannelId, int aSize)
        {
            mRequestId = std::string(aChannelId, aSize);
        }

        void MakeRequest(const std::string& aRequest)
        {
            CURL* curl = NULL;
            curl = curl_easy_init();

            mResponseSize = MakeRequestImpl(curl, aRequest, mYandexApiConfig, mRequestId);

            curl_easy_cleanup(curl);
        }

        size_t GetStreamSize()
        {
            return GetStreamSizeImpl(mResponseBodyStream);
        }

        size_t ReadStream(char* aBuffer, size_t aSize)
        {
            size_t size = mResponseSize - mBytesRead < aSize ? (mResponseSize - mBytesRead) : aSize;
            mResponseBodyStream.read(aBuffer, size);

            mBytesRead += size;

            return size;
        }

        bool IsSuccessed() const
        {
            return mErrorCode == HttpSuccessErrorCode;
        }

        const char* GetSampleRateHertz() const
        {
            return mYandexApiConfig.SampleRateHertz.c_str();
        }

    private:

        void ValidateConfigParam(const char* aName, const std::string& aValue)
        {
            std::cout << "'" << aName << "': " << aValue << "\n";
            if (aValue.empty())
            {
                std::cout << "Empty config '" << aName << "' param\n";
            }
        }

        void ValidateConfigParam(const char* aName, bool aValue)
        {
            std::cout << "'" << aName << "': " << aValue << "\n";
        }

        void Initialize()
        {
            try
            {
                SystemConfig config;

                mYandexApiConfig.ApiKey = Utils::wtos(config->get_config_value<std::wstring>(L"ApiKey"));
                mYandexApiConfig.Url = Utils::wtos(config->get_config_value<std::wstring>(L"TtsServer"));
                mYandexApiConfig.FolderId = Utils::wtos(config->get_config_value<std::wstring>(L"FolderId"));
                mYandexApiConfig.SaveStream = config->safe_get_config_value(L"SaveStreamResponse", false);
                mYandexApiConfig.RawFolder = Utils::wtos(config->get_config_value<std::wstring>(L"RawFolder"));
                mYandexApiConfig.SampleRateHertz = Utils::wtos(config->safe_get_config_value<std::wstring>(L"SampleRateHertz", L"8000"));
                mYandexApiConfig.Lang = Utils::wtos(config->safe_get_config_value<std::wstring>(L"Lang", L"ru-RU"));

                ValidateConfigParam("ApiKey", mYandexApiConfig.ApiKey);
                ValidateConfigParam("Url", mYandexApiConfig.Url);
                ValidateConfigParam("FolderId", mYandexApiConfig.FolderId);
                ValidateConfigParam("SaveStream", mYandexApiConfig.SaveStream);
                ValidateConfigParam("RawFolder", mYandexApiConfig.RawFolder);
                ValidateConfigParam("SampleRateHertz", mYandexApiConfig.SampleRateHertz);
                ValidateConfigParam("Lang", mYandexApiConfig.Lang);

                if (mYandexApiConfig.SampleRateHertz != "8000"
                    && mYandexApiConfig.SampleRateHertz != "16000"
                    && mYandexApiConfig.SampleRateHertz != "48000")
                {
                    throw std::runtime_error("SampleRateHertz available value: '8000' or '16000' or '48000'");
                }
            }
            catch (const std::exception& aError)
            {
                std::cout << aError.what() << std::endl;
                throw aError;
            }
        }

        static size_t GetStreamSizeImpl(std::stringstream& aStream)
        {
            aStream.seekg(0, aStream.end);
            auto size = static_cast<size_t>(aStream.tellg());
            aStream.seekg(0, aStream.beg);

            return size;
        }

        size_t MakeRequestImpl(
            CURL* aCurl, 
            const std::string& aText,
            const YandexApiConfig& aConfig,
            const std::string& aChennelId)
        {
            if (aCurl)
            {
                //std::cout << "Text: " << aText << std::endl;
                std::ofstream f("c:/111.txt", std::ios_base::binary);
                f << aText;
                f.close();


                curl_easy_setopt(aCurl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

                curl_easy_setopt(aCurl, CURLOPT_SSL_VERIFYPEER, 0);
                curl_easy_setopt(aCurl, CURLOPT_POST, 1);

                std::stringstream urlStream;
                urlStream << aConfig.Url << "/speech/v1/tts:synthesize";

                curl_easy_setopt(aCurl, CURLOPT_URL, urlStream.str().c_str());
                struct curl_slist* list = NULL;

                list = curl_slist_append(list, (std::string("Authorization: Api-Key ") + aConfig.ApiKey).c_str());

                curl_easy_setopt(aCurl, CURLOPT_HTTPHEADER, list);

                std::string data = "text=" + aText 
                    + "&lang=" + aConfig.Lang
                    + "&format=lpcm"
                    + "&sampleRateHertz=" + aConfig.SampleRateHertz
                    + "&folderId=" + aConfig.FolderId;

                curl_easy_setopt(aCurl, CURLOPT_POSTFIELDSIZE, data.size());
                curl_easy_setopt(aCurl, CURLOPT_POSTFIELDS, data.c_str());

                curl_easy_setopt(aCurl, CURLOPT_WRITEFUNCTION, write_callback);
                curl_easy_setopt(aCurl, CURLOPT_WRITEDATA, &mResponseBodyStream);

                std::stringstream requestStream;

                curl_easy_setopt(aCurl, CURLOPT_DEBUGFUNCTION, debug_callback);
                curl_easy_setopt(aCurl, CURLOPT_DEBUGDATA, &requestStream);
                curl_easy_setopt(aCurl, CURLOPT_VERBOSE, 1);

                CURLcode code = curl_easy_perform(aCurl);

                curl_slist_free_all(list);

                curl_easy_getinfo(aCurl, CURLINFO_HTTP_CODE, &mErrorCode);
                if (mErrorCode != 200)
                {
                    std::cout << "Request: " << requestStream.str() << std::endl;
                    std::cout << data << std::endl;
                    std::cout << "Respose code is: " << mErrorCode << std::endl;
                    std::cout << "Response: " << mResponseBodyStream.str() << std::endl;
                }
                else
                {
                    if (aConfig.SaveStream)
                    {
                        SaveStreamToFile(aConfig.RawFolder, aChennelId + ".raw", mResponseBodyStream);
                    }
                }
            }
            return GetStreamSizeImpl(mResponseBodyStream);
        }

        static void SaveStreamToFile(const std::string aFolder, const std::string& aFileName, const std::stringstream& aStream)
        {
            if (!boost::filesystem::exists(aFolder) && !boost::filesystem::create_directories(aFolder))
            {
                std::cout << "Cannot create '" << aFolder << "' folder" << std::endl;
                return;
            }

            std::string fileName = aFolder + "\\" + aFileName;
            std::cout << "Save speech to: " << fileName << std::endl;

            std::ofstream f(fileName, std::ios_base::binary);
            f << aStream.str();
            f.close();
        }

    private:
        std::stringstream mResponseBodyStream;
        size_t mBytesRead = 0;
        size_t mResponseSize = 0;
        size_t mErrorCode = 0;

        YandexApiConfig mYandexApiConfig;

        std::string mRequestId;
    };
}