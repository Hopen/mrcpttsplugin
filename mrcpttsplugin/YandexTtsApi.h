#pragma once

typedef struct yandex_tts_stream_t yandex_tts_stream_t;

extern yandex_tts_stream_t* create_tts_stream();
extern void destroy_tts_stream(yandex_tts_stream_t*);

void set_channel_id(yandex_tts_stream_t*, const char*, int);

extern void make_tts_request(yandex_tts_stream_t*, const char*, int);
extern int is_tts_request_successed(yandex_tts_stream_t*);

extern int read_tts_stream(yandex_tts_stream_t*, void*, int);
extern const char* get_sample_rate_hertz(yandex_tts_stream_t*);